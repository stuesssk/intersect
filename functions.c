#include <ctype.h>
#include <stdbool.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <strings.h>

#include "functions.h"

int
fileProcessing(int argc, char *argv[])
// Function to handle the processing of the files and inputing them into
// the appropriate Binary Search Tree
{
    // Initialize my variables
    FILE *fp = NULL;
    char *word = calloc(1, MAX_WORD_SIZE);
    // Counter to determine how many characters have been read in since
    // last whitespace character.
    int length = 0;
    wordNode *original = NULL;
    wordNode *new = NULL;


    for (int i = 1; i < argc; ++i)
    {
        // If unable to open file just end the program
        fp = fopen(argv[i], "r");
        if (!fp)
        {
            printf("Unable to open file %s\n", argv[i]);
            return 1;
        }

        int ch = getc(fp);

        while (isspace(ch))
        {
            // Testing to eliminate any whitespace that may be padding the
            // beginning of the file. If EOF is reached, then the file is all
            // whitespace. And there will be no words from other files
            // that intersect with this file.
            ch = getc(fp);
            if (ch == EOF)
            {
                // File is all whitespace no intersections possible
                printf("Files contain no intersections\n");
                return 1;
            }
        }

        word[length] = ch;
        length++;

        while ((ch != EOF))
        {
            // Reading in each character
            ch = getc(fp);
            word[length] = ch;

            // Don't want the newline in final result. overwrite with NULL
            if (ch == '\n' || isspace(ch))
            {
                word[length] = '\0';
            }

            // Handles the case if there are multiple counts of whitespace
            // between words. If true this will skip it.
            if (word[0] == '\0')
            {
                continue;
            }

            // Once whitespace is encounterd store that word in the BST
            if (isspace(ch))
            {
                // Comparision to test if word is in BST derived from 
                // Alex Dow to fix issue with words not being inserted 
                // properly into BST
                if (i == 1)
                {
                    // Store all words for first file. Only checking 
                    // for dulpicates.
                    if (!compareWords(original, word))
                    {
                        original = insertWord(original, word);
                    }
                }
                else
                {
                    // If word from other files is in intersect store into
                    // new intersect file
                    if (compareWords(original, word))
                    {
                        new = insertWord(new, word);
                    }
                }
                // Resetting variables to ensure your not storing values
                // from other words in the file 
                memset(word, '\0', strlen(word));
                length = 0;
                continue;
            }
            length++;
        }
        if (i > 1)
        {
            // Setting original to new BST to be able to compare 
            // more files.  This could not have been done, by me, with out
            // help of D. Ems.  His help was vital to me being able to 
            // comprehend the steps needed to asign the new BST to the original
            // BST, without losing a handle on the new (the BST tracking the 
            // intersects of all the files).
            destroy_BST(original);
            original = NULL;
            original = new;
            new = NULL;
        }
        // Close the file
        fclose(fp);
    }
    // Give the customer what they want. Print the intersect of the files
    printWordList(original);
    // Must free all the mallocs Valgrind overloard commands it
    destroy_BST(original);
    free(word);
    return 0;
}


void
destroy_BST(wordNode * root)
{
// Destroy function modified from Class source code on BST function
// Function will free any memory that was malloc in the creation of the BST
    if (root == NULL)
    {
        return;
    }
    // Don't need it any more, kill it all.
    destroy_BST(root->left);
    destroy_BST(root->right);
    free(root->data);
    free(root);
}

wordNode*
insertWord(wordNode * root, char *word)
{
// Insert function modified from Class source code on BST function
// Function will insert a node into the appropriate place int the BST
    int comparison = 0;

    if (root == NULL)
    {
        // Word is in proper place, calloc space for it.
        root = calloc(1, sizeof(*root));
        if (root == NULL)
        {
            printf("Memory Error");
            return NULL;
        }
        else
        {
            // Storing data into node
            root->data = calloc(1, MAX_WORD_SIZE);
            strncpy(root->data, word, strlen(word));
            root->left = NULL;
            root->right = NULL;
        }
    }
    else
    {
        // Identify if word is less than or greater than root->data
        // if equal function will drop it.
        comparison = strcasecmp(word, root->data);
        if (comparison < 0)
        {
            root->left = insertWord(root->left, word);
        }
        else if (comparison > 0)
        {
            root->right = insertWord(root->right, word);
        }
    }
    return root;
}

void
printWordList(wordNode * root)
{
// Print function modified from Class source code on BST function
// Print function will print the contents of the BST in order, from
// lowest value to highest. In the case of this program, in alphabetical order
    if (root == NULL)
    {
        return;
    }

    printWordList(root->left);
    printf("%s\n", root->data);
    printWordList(root->right);
}

bool
compareWords(wordNode * new, char *word)
{
// This function will test if the string passed exists in the BST passed to the
// function.  The function will ignore the case of both words during the 
// comparision
    bool match = false;

    if (new)
    {
        // Need to inverse, since strcasecmp returns 0 if the 
        // strings are equal.
        if (!strcasecmp(word, new->data))
        {
            match = true;
            return match;
        }

        match = compareWords(new->left, word);

        if (!match)
        {
            match = compareWords(new->right, word);
        }
    }
    return match;
}
