#include <stdio.h>
#include <sysexits.h>
#include <sys/stat.h>


#include "functions.h"

int
main(int argc, char *argv[])
{
    // You done screwed up if I don't see at least three comamand line
    // arguments come through
    if (argc < 3)
    {
        fprintf(stderr, "Usage: %s <File1> <File2> (File3...)\n", argv[0]);
        return EX_USAGE;
    }

    struct stat st;

    for (int i = 1; i < argc; ++i)
    {
        // Testing for file of size 0. If any file is size 0 bytes, there
        // is guranteed to be no intersects.
        stat(argv[i], &st);
        if (st.st_size == 0)
        {
            printf("No intersects.\n");
            return 0;
        }
    }

    int test = fileProcessing(argc, argv);
    if (test)
    {
        return EX_NOINPUT;
    }
    return EX_OK;
}
