#ifndef INTERSECT_H
#define INTERSECT_H

#include <stdbool.h>

typedef struct BSTWord
{
    char *data;
    struct BSTWord *left;
    struct BSTWord *right;
}wordNode;

enum
{
    MAX_WORD_SIZE = 256,
};

int
fileProcessing(int argc, char *argv[]);

void
destroy_BST(wordNode* root);

wordNode *
insertWord(wordNode *root, char * data);

void
printWordList(wordNode *root);

bool
compareWords(wordNode *new, char *word);

#endif
